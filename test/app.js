'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const app = express();

const dummyContent = {
	'text/plain': 'plain text',
	'text/html': '<html><body><p>Hypertext content.</p></body></html>',
	'application.json': JSON.stringify( { text: 'text' } )
};

app.use(bodyParser.json( { type: 'application/json' } ));
app.use(bodyParser.text( { type: 'text/*' } ));
app.use(bodyParser.urlencoded({ extended: true, type: 'application/x-www-form-urlencoded' }));

app.get('/echo', (req, res) => {
	const type = req.query.type || 'application/json';
	const content = req.query.content || dummyContent[type];
	const status = req.query.status ? parseInt( req.query.status ) : 200;

	res.status( status );
	res.setHeader('content-type', type );
	res.setHeader('user-agent', req.headers['user-agent'] );

	res.send(content);
});

app.post('/echo', (req, res) => {
	const type = req.headers['content-type'] || 'application/json';
	const content = req.body || dummyContent[type];
	const status = req.query.status ? parseInt( req.query.status ) : 200;

	res.status( status );
	res.setHeader('content-type', type );
	res.setHeader('user-agent', req.headers['user-agent'] );

	res.send(content);
});

/*
app.listen(port, () => {
	console.log(`Example app listening on port ${port}`);
})
*/

module.exports = app;
