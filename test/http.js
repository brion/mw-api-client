'use strict';

const { HttpClient } = require('../index');
const app = require('./app');
const supertest = require('supertest');
const expect = require('chai').expect;

const noOptions = {};

describe('HTTP Request', () => {

	it('should support get', async () => {
		const client = new HttpClient( noOptions, supertest(app) );

		const request = client.get('/echo')
			.set( 'foo', 'bar')
			.then( ( resp ) => {
				resp.extra = true;
				return resp;
			} );

		// FIXME: this returns undefined. Something is wrong with the fake "then".
		const response = await request;

		expect(response.statusCode).to.equal(200, response.text);
		expect(response).to.have.property('apiErrors');
		expect(response).to.have.property('apiWarnings');
		expect(response).to.have.property('extra');

		expect(response.headers)
			.to.have.property('user-agent')
			.which.matches(/test/);
	});

	it('should support post', async () => {
		const client = new HttpClient( noOptions, supertest(app) );

		const response = await client.post('/echo')
			.type('text/plain')
			.send('some text');

		expect(response.statusCode).to.equal(200, response.text);
		expect(response).to.have.property('apiErrors');
		expect(response).to.have.property('apiWarnings');

		expect(response.text).to.equal('some text');
		expect(response.headers['content-type']).to.match(/^text\/plain/);
	});

	it('should report a 404', async () => {
		const client = new HttpClient( noOptions, supertest(app) );

		const response = await client.get('/echo')
			.query( { status: 404 } );

		expect(response.statusCode).to.equal(404, response.text);
		expect(response).to.have.property('apiErrors');
		expect(response).to.have.property('apiWarnings');
	});

});
