'use strict';

const superagent = require('superagent');
// const utils = require('./utils');

// FIXME: inspect prototype to get them all
const _httpMethods = [ 'get', 'put', 'post', 'head', 'delete' ];

function _getMethodNames( obj ) {
	return Object.getOwnPropertyNames(obj)
		.filter( (item) => typeof obj[item] === 'function');
}

function _delay( ms ) {
	return new Promise( (resolve, reject) => {
		setTimeout(() => resolve(), ms);
	} );
}

/*
function _delegate( wrapper, base ) {
  const properties = Object.getOwnPropertyNames(base);
	properties.forEach( (name) => {
		if ( name.startsWith( '_' ) ) {
			return;
		}

    if ( typeof base[name] === 'function' ) {
			if ( wrapper[name] ) {
				return;
			}

			wrapper[name] = function (...args) {
				console.log( `--> .${name}(${args})` );
        return base[name].call(request, ...args);
      };
    } else {
			Object.defineProperty( wrapper, name, {
				get: () => {
					console.log( `<== .${name}` );
					return base[name];
				},
				set: ( value ) => {
					console.log( `==> .${name} = ${value}` );
					base[name] = value;
				},
			} );
		}
  } );

	return wrapper;
}
*/

class HttpClient {

	/**
	 * Constructs a new agent for making HTTP requests to a MediaWiki API.
	 *
	 * @param {Object} config
	 * @param {Object} agent superagent agent
	 */
	constructor(config, agent) {
		this.config = config;
		this.superagent = agent || superagent.agent(config.base_uri);

		// TODO: shared cookies
		// TODO: shared throttles (plural!)

		// Delegate agent methods
		const agentMethods = _getMethodNames( this.superagent );
		agentMethods.forEach( (name) => {
			if ( !this[name] ) {
				this[name] = this._wrapAgentMethod( name, this.superagent[name] );
			}
		} );
	}

	_wrapAgentMethod(name, func ) {
		if ( _httpMethods.includes( name ) ) {
			return this._wrapHttpMethod( name, func );
		} else {
			return function (...args) {
				return func.call(this.superagent, ...args);
			};
		}
	}

	// TODO: follow( 304, ...) // follow certain kinds of redirects (default: all)
	// TODO: allow( 404, ...) // don't throw on status (default: throw on all)
	// TODO: strict() // fail on warnings (for test, mostly)

	_wrapHttpMethod( name, func ) {
		return function (...args) {
			const request = func.call(this.superagent, ...args);

			// TODO: set user agent from config
			// TODO: set Api-User-Agent in browser!
			request.set('user-agent', 'just a test'); // FIXME

			// TODO: trigger retry
			// TODO: handle timeout

			const oldThen = request.then;
			const _this = this;
			request.then = ( resolve, reject ) => _this._onThen(request, oldThen, resolve, reject);

			return request;
		}
	}

	_onThen( request, oldThen, resolve, reject ) {
		console.log('THEN');
		// TODO: apply throttle
		return _delay(100).then(() => {
			const resp = oldThen.call( request, resolve, reject );
			console.log('RESP 1', resp.toString());
			return resp;
		}).then((resp) => {
			console.log('RESP 2', resp.toString());
			// TODO: record errors and warnings (even across redirects!)
			// TODO: treat permanent redirects as warnings
			// TODO: log errors and warnings
			// TODO: retry...
			resp.apiErrors = [];
			resp.apiWarnings = [];

			// TODO: throw on status
			return resp;
		}).catch((error) => {
			console.log( 'ERROR', error );
			throw error;
		});
	}

}
module.exports = HttpClient;
